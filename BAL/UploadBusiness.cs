﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;

namespace IMUPRO.UI
{
    public class UploadBusiness : DapperBase
    {
        public void UploadData(List<Patient> lstPatient, SqlTransaction trans = null)
        {
            int count = lstPatient.Count();
            for (int i = 0; i < count; i++)
            {
                Conn.Execute(SP_UPLOAD_SAVE_DATA, lstPatient[i], trans, commandType: CommandType.StoredProcedure);
            }

        }

        static readonly string SP_UPLOAD_SAVE_DATA = "SP_UPLOAD_SAVE_DATA";
    }
}