﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;

namespace IMUPRO.UI
{
    public class ReportBusiness : DapperBase
    {
        public SR<List<ReportModel>> GetCategoryWiseReport()
        {
            List<ReportModel> result = new List<ReportModel>();
            result = Conn.Query<ReportModel>(SP_GET_CATEGORY_WISE_REPORT, transaction: null, commandType: CommandType.StoredProcedure).ToList();
            return new SR<List<ReportModel>>() { Result = result };
        }

        public static readonly string SP_GET_CATEGORY_WISE_REPORT = "SP_SELECT_PATIENT_DATA";
    }
}