﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMUPRO.UI
{
    public class ExportToExcel
    {
        private Application excelApp;
        private Workbook newWorkbook;
        private Worksheet xlWorkSheet;
        private string filePath;

        public ExportToExcel(string m_filePath)
        {
            excelApp = new Application();
            filePath = m_filePath;
        }

        private bool CreateWorkbook()
        {
            bool rc = false;
            if (excelApp != null)
            {
                newWorkbook = excelApp.Workbooks.Open(filePath);
                rc = true;
            }
            return rc;
        }

        public void WriteDataInSheet(List<ReportModel> lstReport)
        {
            try
            {
                if (CreateWorkbook())
                {
                    xlWorkSheet = (Worksheet)newWorkbook.Worksheets.get_Item(1);
                    if (xlWorkSheet != null)
                    {
                        //System.Data.DataTable dtColumns = ExcelToDataTable.GetReportColumns(filePath);

                        int rowCount = lstReport.Count();
                        int columnCount = 269;

                        int innerColumn = 0;
                        int key = 0;

                        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++)
                        {
                            innerColumn = 10;
                            key = 1;

                            xlWorkSheet.Cells[rowIndex + 2, 1] = (rowIndex + 1).ToString();
                            xlWorkSheet.Cells[rowIndex + 2, 2] = lstReport[rowIndex].SessionCode;
                            xlWorkSheet.Cells[rowIndex + 2, 3] = lstReport[rowIndex].Name;
                            xlWorkSheet.Cells[rowIndex + 2, 4] = lstReport[rowIndex].Sex;
                            xlWorkSheet.Cells[rowIndex + 2, 5] = lstReport[rowIndex].DOB;
                            xlWorkSheet.Cells[rowIndex + 2, 6] = lstReport[rowIndex].Hospital;
                            xlWorkSheet.Cells[rowIndex + 2, 7] = lstReport[rowIndex].Doctor;
                            xlWorkSheet.Cells[rowIndex + 2, 8] = lstReport[rowIndex].FoodNo;
                            xlWorkSheet.Cells[rowIndex + 2, 9] = lstReport[rowIndex].TestDate;

                            for (int columnIndex = 1; columnIndex < columnCount; columnIndex++)
                            {
                                switch ("c_" + columnIndex.ToString())
                                {
                                    case "c_1": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_1; key++; innerColumn++; break;
                                    case "c_2": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_2; key++; innerColumn++; break;
                                    case "c_3": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_3; key++; innerColumn++; break;
                                    case "c_4": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_4; key++; innerColumn++; break;
                                    case "c_5": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_5; key++; innerColumn++; break;
                                    case "c_6": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_6; key++; innerColumn++; break;
                                    case "c_7": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_7; key++; innerColumn++; break;
                                    case "c_8": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_8; key++; innerColumn++; break;
                                    case "c_9": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_9; key++; innerColumn++; break;
                                    case "c_10": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_10; key++; innerColumn++; break;
                                    case "c_11": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_11; key++; innerColumn++; break;
                                    case "c_12": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_12; key++; innerColumn++; break;
                                    case "c_13": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_13; key++; innerColumn++; break;
                                    case "c_14": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_14; key++; innerColumn++; break;
                                    case "c_15": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_15; key++; innerColumn++; break;
                                    case "c_16": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_16; key++; innerColumn++; break;
                                    case "c_17": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_17; key++; innerColumn++; break;
                                    case "c_18": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_18; key++; innerColumn++; break;
                                    case "c_19": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_19; key++; innerColumn++; break;
                                    case "c_20": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_20; key++; innerColumn++; break;
                                    case "c_21": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_21; key++; innerColumn++; break;
                                    case "c_22": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_22; key++; innerColumn++; break;
                                    case "c_23": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_23; key++; innerColumn++; break;
                                    case "c_24": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_24; key++; innerColumn++; break;
                                    case "c_25": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_25; key++; innerColumn++; break;
                                    case "c_26": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_26; key++; innerColumn++; break;
                                    case "c_27": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_27; key++; innerColumn++; break;
                                    case "c_28": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_28; key++; innerColumn++; break;
                                    case "c_29": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_29; key++; innerColumn++; break;
                                    case "c_30": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_30; key++; innerColumn++; break;
                                    case "c_31": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_31; key++; innerColumn++; break;
                                    case "c_32": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_32; key++; innerColumn++; break;
                                    case "c_33": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_33; key++; innerColumn++; break;
                                    case "c_34": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_34; key++; innerColumn++; break;
                                    case "c_35": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_35; key++; innerColumn++; break;
                                    case "c_36": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_36; key++; innerColumn++; break;
                                    case "c_37": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_37; key++; innerColumn++; break;
                                    case "c_38": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_38; key++; innerColumn++; break;
                                    case "c_39": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_39; key++; innerColumn++; break;
                                    case "c_40": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_40; key++; innerColumn++; break;
                                    case "c_41": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_41; key++; innerColumn++; break;
                                    case "c_42": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_42; key++; innerColumn++; break;
                                    case "c_43": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_43; key++; innerColumn++; break;
                                    case "c_44": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_44; key++; innerColumn++; break;
                                    case "c_45": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_45; key++; innerColumn++; break;
                                    case "c_46": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_46; key++; innerColumn++; break;
                                    case "c_47": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_47; key++; innerColumn++; break;
                                    case "c_48": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_48; key++; innerColumn++; break;
                                    case "c_49": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_49; key++; innerColumn++; break;
                                    case "c_50": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_50; key++; innerColumn++; break;
                                    case "c_51": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_51; key++; innerColumn++; break;
                                    case "c_52": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_52; key++; innerColumn++; break;
                                    case "c_53": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_53; key++; innerColumn++; break;
                                    case "c_54": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_54; key++; innerColumn++; break;
                                    case "c_55": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_55; key++; innerColumn++; break;
                                    case "c_56": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_56; key++; innerColumn++; break;
                                    case "c_57": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_57; key++; innerColumn++; break;
                                    case "c_58": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_58; key++; innerColumn++; break;
                                    case "c_59": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_59; key++; innerColumn++; break;
                                    case "c_60": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_60; key++; innerColumn++; break;
                                    case "c_61": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_61; key++; innerColumn++; break;
                                    case "c_62": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_62; key++; innerColumn++; break;
                                    case "c_63": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_63; key++; innerColumn++; break;
                                    case "c_64": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_64; key++; innerColumn++; break;
                                    case "c_65": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_65; key++; innerColumn++; break;
                                    case "c_66": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_66; key++; innerColumn++; break;
                                    case "c_67": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_67; key++; innerColumn++; break;
                                    case "c_68": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_68; key++; innerColumn++; break;
                                    case "c_69": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_69; key++; innerColumn++; break;
                                    case "c_70": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_70; key++; innerColumn++; break;
                                    case "c_71": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_71; key++; innerColumn++; break;
                                    case "c_72": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_72; key++; innerColumn++; break;
                                    case "c_73": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_73; key++; innerColumn++; break;
                                    case "c_74": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_74; key++; innerColumn++; break;
                                    case "c_75": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_75; key++; innerColumn++; break;
                                    case "c_76": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_76; key++; innerColumn++; break;
                                    case "c_77": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_77; key++; innerColumn++; break;
                                    case "c_78": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_78; key++; innerColumn++; break;
                                    case "c_79": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_79; key++; innerColumn++; break;
                                    case "c_80": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_80; key++; innerColumn++; break;
                                    case "c_81": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_81; key++; innerColumn++; break;
                                    case "c_82": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_82; key++; innerColumn++; break;
                                    case "c_83": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_83; key++; innerColumn++; break;
                                    case "c_84": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_84; key++; innerColumn++; break;
                                    case "c_85": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_85; key++; innerColumn++; break;
                                    case "c_86": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_86; key++; innerColumn++; break;
                                    case "c_87": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_87; key++; innerColumn++; break;
                                    case "c_88": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_88; key++; innerColumn++; break;
                                    case "c_89": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_89; key++; innerColumn++; break;
                                    case "c_90": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_90; key++; innerColumn++; break;
                                    case "c_91": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_91; key++; innerColumn++; break;
                                    case "c_92": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_92; key++; innerColumn++; break;
                                    case "c_93": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_93; key++; innerColumn++; break;
                                    case "c_94": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_94; key++; innerColumn++; break;
                                    case "c_95": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_95; key++; innerColumn++; break;
                                    case "c_96": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_96; key++; innerColumn++; break;
                                    case "c_97": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_97; key++; innerColumn++; break;
                                    case "c_98": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_98; key++; innerColumn++; break;
                                    case "c_99": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_99; key++; innerColumn++; break;
                                    case "c_100": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_100; key++; innerColumn++; break;
                                    case "c_101": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_101; key++; innerColumn++; break;
                                    case "c_102": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_102; key++; innerColumn++; break;
                                    case "c_103": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_103; key++; innerColumn++; break;
                                    case "c_104": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_104; key++; innerColumn++; break;
                                    case "c_105": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_105; key++; innerColumn++; break;
                                    case "c_106": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_106; key++; innerColumn++; break;
                                    case "c_107": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_107; key++; innerColumn++; break;
                                    case "c_108": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_108; key++; innerColumn++; break;
                                    case "c_109": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_109; key++; innerColumn++; break;
                                    case "c_110": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_110; key++; innerColumn++; break;
                                    case "c_111": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_111; key++; innerColumn++; break;
                                    case "c_112": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_112; key++; innerColumn++; break;
                                    case "c_113": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_113; key++; innerColumn++; break;
                                    case "c_114": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_114; key++; innerColumn++; break;
                                    case "c_115": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_115; key++; innerColumn++; break;
                                    case "c_116": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_116; key++; innerColumn++; break;
                                    case "c_117": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_117; key++; innerColumn++; break;
                                    case "c_118": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_118; key++; innerColumn++; break;
                                    case "c_119": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_119; key++; innerColumn++; break;
                                    case "c_120": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_120; key++; innerColumn++; break;
                                    case "c_121": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_121; key++; innerColumn++; break;
                                    case "c_122": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_122; key++; innerColumn++; break;
                                    case "c_123": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_123; key++; innerColumn++; break;
                                    case "c_124": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_124; key++; innerColumn++; break;
                                    case "c_125": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_125; key++; innerColumn++; break;
                                    case "c_126": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_126; key++; innerColumn++; break;
                                    case "c_127": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_127; key++; innerColumn++; break;
                                    case "c_128": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_128; key++; innerColumn++; break;
                                    case "c_129": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_129; key++; innerColumn++; break;
                                    case "c_130": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_130; key++; innerColumn++; break;
                                    case "c_131": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_131; key++; innerColumn++; break;
                                    case "c_132": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_132; key++; innerColumn++; break;
                                    case "c_133": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_133; key++; innerColumn++; break;
                                    case "c_134": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_134; key++; innerColumn++; break;
                                    case "c_135": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_135; key++; innerColumn++; break;
                                    case "c_136": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_136; key++; innerColumn++; break;
                                    case "c_137": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_137; key++; innerColumn++; break;
                                    case "c_138": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_138; key++; innerColumn++; break;
                                    case "c_139": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_139; key++; innerColumn++; break;
                                    case "c_140": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_140; key++; innerColumn++; break;
                                    case "c_141": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_141; key++; innerColumn++; break;
                                    case "c_142": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_142; key++; innerColumn++; break;
                                    case "c_143": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_143; key++; innerColumn++; break;
                                    case "c_144": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_144; key++; innerColumn++; break;
                                    case "c_145": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_145; key++; innerColumn++; break;
                                    case "c_146": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_146; key++; innerColumn++; break;
                                    case "c_147": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_147; key++; innerColumn++; break;
                                    case "c_148": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_148; key++; innerColumn++; break;
                                    case "c_149": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_149; key++; innerColumn++; break;
                                    case "c_150": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_150; key++; innerColumn++; break;
                                    case "c_151": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_151; key++; innerColumn++; break;
                                    case "c_152": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_152; key++; innerColumn++; break;
                                    case "c_153": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_153; key++; innerColumn++; break;
                                    case "c_154": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_154; key++; innerColumn++; break;
                                    case "c_155": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_155; key++; innerColumn++; break;
                                    case "c_156": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_156; key++; innerColumn++; break;
                                    case "c_157": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_157; key++; innerColumn++; break;
                                    case "c_158": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_158; key++; innerColumn++; break;
                                    case "c_159": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_159; key++; innerColumn++; break;
                                    case "c_160": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_160; key++; innerColumn++; break;
                                    case "c_161": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_161; key++; innerColumn++; break;
                                    case "c_162": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_162; key++; innerColumn++; break;
                                    case "c_163": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_163; key++; innerColumn++; break;
                                    case "c_164": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_164; key++; innerColumn++; break;
                                    case "c_165": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_165; key++; innerColumn++; break;
                                    case "c_166": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_166; key++; innerColumn++; break;
                                    case "c_167": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_167; key++; innerColumn++; break;
                                    case "c_168": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_168; key++; innerColumn++; break;
                                    case "c_169": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_169; key++; innerColumn++; break;
                                    case "c_170": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_170; key++; innerColumn++; break;
                                    case "c_171": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_171; key++; innerColumn++; break;
                                    case "c_172": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_172; key++; innerColumn++; break;
                                    case "c_173": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_173; key++; innerColumn++; break;
                                    case "c_174": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_174; key++; innerColumn++; break;
                                    case "c_175": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_175; key++; innerColumn++; break;
                                    case "c_176": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_176; key++; innerColumn++; break;
                                    case "c_177": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_177; key++; innerColumn++; break;
                                    case "c_178": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_178; key++; innerColumn++; break;
                                    case "c_179": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_179; key++; innerColumn++; break;
                                    case "c_180": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_180; key++; innerColumn++; break;
                                    case "c_181": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_181; key++; innerColumn++; break;
                                    case "c_182": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_182; key++; innerColumn++; break;
                                    case "c_183": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_183; key++; innerColumn++; break;
                                    case "c_184": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_184; key++; innerColumn++; break;
                                    case "c_185": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_185; key++; innerColumn++; break;
                                    case "c_186": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_186; key++; innerColumn++; break;
                                    case "c_187": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_187; key++; innerColumn++; break;
                                    case "c_188": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_188; key++; innerColumn++; break;
                                    case "c_189": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_189; key++; innerColumn++; break;
                                    case "c_190": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_190; key++; innerColumn++; break;
                                    case "c_191": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_191; key++; innerColumn++; break;
                                    case "c_192": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_192; key++; innerColumn++; break;
                                    case "c_193": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_193; key++; innerColumn++; break;
                                    case "c_194": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_194; key++; innerColumn++; break;
                                    case "c_195": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_195; key++; innerColumn++; break;
                                    case "c_196": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_196; key++; innerColumn++; break;
                                    case "c_197": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_197; key++; innerColumn++; break;
                                    case "c_198": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_198; key++; innerColumn++; break;
                                    case "c_199": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_199; key++; innerColumn++; break;
                                    case "c_200": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_200; key++; innerColumn++; break;
                                    case "c_201": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_201; key++; innerColumn++; break;
                                    case "c_202": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_202; key++; innerColumn++; break;
                                    case "c_203": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_203; key++; innerColumn++; break;
                                    case "c_204": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_204; key++; innerColumn++; break;
                                    case "c_205": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_205; key++; innerColumn++; break;
                                    case "c_206": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_206; key++; innerColumn++; break;
                                    case "c_207": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_207; key++; innerColumn++; break;
                                    case "c_208": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_208; key++; innerColumn++; break;
                                    case "c_209": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_209; key++; innerColumn++; break;
                                    case "c_210": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_210; key++; innerColumn++; break;
                                    case "c_211": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_211; key++; innerColumn++; break;
                                    case "c_212": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_212; key++; innerColumn++; break;
                                    case "c_213": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_213; key++; innerColumn++; break;
                                    case "c_214": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_214; key++; innerColumn++; break;
                                    case "c_215": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_215; key++; innerColumn++; break;
                                    case "c_216": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_216; key++; innerColumn++; break;
                                    case "c_217": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_217; key++; innerColumn++; break;
                                    case "c_218": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_218; key++; innerColumn++; break;
                                    case "c_219": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_219; key++; innerColumn++; break;
                                    case "c_220": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_220; key++; innerColumn++; break;
                                    case "c_221": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_221; key++; innerColumn++; break;
                                    case "c_222": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_222; key++; innerColumn++; break;
                                    case "c_223": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_223; key++; innerColumn++; break;
                                    case "c_224": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_224; key++; innerColumn++; break;
                                    case "c_225": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_225; key++; innerColumn++; break;
                                    case "c_226": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_226; key++; innerColumn++; break;
                                    case "c_227": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_227; key++; innerColumn++; break;
                                    case "c_228": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_228; key++; innerColumn++; break;
                                    case "c_229": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_229; key++; innerColumn++; break;
                                    case "c_230": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_230; key++; innerColumn++; break;
                                    case "c_231": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_231; key++; innerColumn++; break;
                                    case "c_232": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_232; key++; innerColumn++; break;
                                    case "c_233": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_233; key++; innerColumn++; break;
                                    case "c_234": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_234; key++; innerColumn++; break;
                                    case "c_235": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_235; key++; innerColumn++; break;
                                    case "c_236": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_236; key++; innerColumn++; break;
                                    case "c_237": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_237; key++; innerColumn++; break;
                                    case "c_238": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_238; key++; innerColumn++; break;
                                    case "c_239": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_239; key++; innerColumn++; break;
                                    case "c_240": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_240; key++; innerColumn++; break;
                                    case "c_241": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_241; key++; innerColumn++; break;
                                    case "c_242": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_242; key++; innerColumn++; break;
                                    case "c_243": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_243; key++; innerColumn++; break;
                                    case "c_244": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_244; key++; innerColumn++; break;
                                    case "c_245": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_245; key++; innerColumn++; break;
                                    case "c_246": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_246; key++; innerColumn++; break;
                                    case "c_247": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_247; key++; innerColumn++; break;
                                    case "c_248": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_248; key++; innerColumn++; break;
                                    case "c_249": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_249; key++; innerColumn++; break;
                                    case "c_250": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_250; key++; innerColumn++; break;
                                    case "c_251": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_251; key++; innerColumn++; break;
                                    case "c_252": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_252; key++; innerColumn++; break;
                                    case "c_253": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_253; key++; innerColumn++; break;
                                    case "c_254": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_254; key++; innerColumn++; break;
                                    case "c_255": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_255; key++; innerColumn++; break;
                                    case "c_256": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_256; key++; innerColumn++; break;
                                    case "c_257": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_257; key++; innerColumn++; break;
                                    case "c_258": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_258; key++; innerColumn++; break;
                                    case "c_259": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_259; key++; innerColumn++; break;
                                    case "c_260": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_260; key++; innerColumn++; break;
                                    case "c_261": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_261; key++; innerColumn++; break;
                                    case "c_262": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_262; key++; innerColumn++; break;
                                    case "c_263": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_263; key++; innerColumn++; break;
                                    case "c_264": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_264; key++; innerColumn++; break;
                                    case "c_265": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_265; key++; innerColumn++; break;
                                    case "c_266": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_266; key++; innerColumn++; break;
                                    case "c_267": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_267; key++; innerColumn++; break;
                                    case "c_268": xlWorkSheet.Cells[rowIndex + 2, innerColumn] = lstReport[rowIndex].c_268; key++; innerColumn++; break;
                                }
                            }
                        }
                    }

                    excelApp.Visible = true;
                    excelApp.WindowState = XlWindowState.xlMaximized;
                    excelApp.Workbooks.Close();
                }
            }
            catch
            {
                throw;
            }
        }
    }
}