﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.pdf.parser;
using System.Collections;
using System.Text.RegularExpressions;
using System.Data;

namespace IMUPRO.UI.BAL
{
    public class pdfToTable
    {

        public static string filePath = HttpContext.Current.Server.MapPath("~/FileUpload/");

        public static Patient PDFData(string pdfdata)
        {
            Patient objpPat = new Patient();

            string toFind1 = "Patient";
            int start = pdfdata.IndexOf(toFind1) + toFind1.Length;
            string string2 = pdfdata.Substring(start);
            string patientName = string2.Substring(1, string2.IndexOf("Date of Test"));

            int foodsindex = string2.IndexOf("foods:") + 7;
            ArrayList ae = new ArrayList();
            List<string> str = new List<string>();

            objpPat.PatientName = patientName;
            objpPat.PatientDOB = pdfdata.Substring(pdfdata.IndexOf("Date of birth") + 14, pdfdata.IndexOf("Date") - 20);
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[3] { new DataColumn("F1"), new DataColumn("F3"), new DataColumn("F2") });

            for (int i = foodsindex; i < string2.Length; i++)
            {
                if (string2[i].ToString() == ")" && Regex.IsMatch(string2[i - 1].ToString(), @"^\d+$"))
                {
                    string sto = string2.Substring(foodsindex, i - (foodsindex - 1));

                    dt.Rows.Add(sto.Substring(0, sto.IndexOf("(")), Regex.Match(sto, @"\d+").Value, "");
                    // str.Add(sto);
                    foodsindex = i + 1;

                }
            }

            objpPat.PositiveTest = dt.Copy();


            return objpPat;
        }



        public static string ExtractTextFromPdf(string file)
        {
            string path = filePath + file;
            using (PdfReader reader = new PdfReader(path))
            {
                StringBuilder text = new StringBuilder();

                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                }

                return text.ToString();
            }
        }


    }
}