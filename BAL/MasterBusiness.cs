﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace IMUPRO.UI
{
    public class MasterBusiness : DapperBase
    {
        public Dictionary<string, List<IMUPROSelectList>> GetSlimEntities(SqlTransaction trans = null)
        {
            var dictionary = new Dictionary<string, List<IMUPROSelectList>>();

            using (var multi = Conn.QueryMultiple(GET_SLIM_ENTITIES, transaction: trans, commandType: CommandType.Text))
            {
                dictionary.Add("Category", multi.Read<IMUPROSelectList>().ToList());
                dictionary.Add("SubCategory", multi.Read<IMUPROSelectList>().ToList());
                dictionary.Add("Food", multi.Read<IMUPROSelectList>().ToList());
            }

            return dictionary;
        }

        static string GET_SLIM_ENTITIES = @"Select Id as value,Category as name from Category
                                            Select Id as value,SubCategory as name from SubCategory
                                            Select Id as value,Food as name from Foods";
    }
}