﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace IMUPRO.UI
{
    public class ExcelToDataTable
    {
        public static string filePath = HttpContext.Current.Server.MapPath("~/FileUpload/");

        public static DataTable ExcelData(List<string> files)
        {
            DataTable dtexcel = new DataTable();
            int count = files.Count();
            for (int i = 0; i < count; i++)
            {
                string file = filePath + files[i];
                bool hasHeaders = false;
                string HDR = hasHeaders ? "Yes" : "No";
                string strConn;
                if (file.Substring(file.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                else
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + file + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                //Looping Total Sheet of Xl File
                /*foreach (DataRow schemaRow in schemaTable.Rows)
                {
                }*/
                //Looping a first Sheet of Xl File
                DataRow schemaRow = schemaTable.Rows[0];
                string sheet = schemaRow["TABLE_NAME"].ToString();
                if (!sheet.EndsWith("_"))
                {
                    string query = "SELECT  * FROM [" + sheet + "]";
                    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    dtexcel.Locale = CultureInfo.CurrentCulture;
                    daexcel.Fill(dtexcel);
                }

                conn.Close();
            }
            return dtexcel;
        }

        public static Patient ExtractDataFromDataTable(DataTable dtData)
        {
            Patient objPatient = new Patient();
            int columnCount = dtData.Columns.Count;
            if (columnCount != 3)
                throw new Exception("Invalid File");

            string[] arrNameDOBCodeData = dtData.Rows[0][0].ToString().Split(':');

            if (arrNameDOBCodeData.Length != 4)
                throw new Exception("Invalid Data");

            objPatient.PatientName = arrNameDOBCodeData[1].Replace("Date of birth", "").Trim();
            objPatient.PatientDOB = arrNameDOBCodeData[2].Replace("Session code", "").Trim();

            //dd.mm.yyyy
            string[] arrDOB = objPatient.PatientDOB.Split('.');
            if (arrDOB.Length != 3)
                throw new Exception();

            objPatient.PatientDOB = new DateTime(Convert.ToInt32(arrDOB[2]), Convert.ToInt32(arrDOB[1]), Convert.ToInt32(arrDOB[0])).ToShortDateString();
            objPatient.SessionCode = arrNameDOBCodeData[3].Trim();

            string noOfIncompatibleFoods = dtData.Rows[1][0].ToString();

            if (!noOfIncompatibleFoods.Contains("Number of incompatible foods"))
                throw new Exception("Invalid Data");

            string[] arrIncompatibleFoods = noOfIncompatibleFoods.Split(':');
            if (arrIncompatibleFoods.Length != 2)
                throw new Exception("Invalid Data");

            objPatient.IncompatibleFoodsCount = Convert.ToInt32(arrIncompatibleFoods[1]);

            DataRow[] dr = dtData.Select("F3<>'Result' and F3<>''");
            if (dr.Length > 0)
                objPatient.PositiveTest = dr.CopyToDataTable();

            return objPatient;
        }

        public static DataTable GetReportColumns(string file)
        {
            DataTable dtexcel = new DataTable();
            bool hasHeaders = false;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (file.Substring(file.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            else
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + file + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            //Looping Total Sheet of Xl File
            /*foreach (DataRow schemaRow in schemaTable.Rows)
            {
            }*/
            //Looping a first Sheet of Xl File
            DataRow schemaRow = schemaTable.Rows[0];
            string sheet = schemaRow["TABLE_NAME"].ToString();
            if (!sheet.EndsWith("_"))
            {
                string query = "SELECT  * FROM [" + sheet + "]";
                OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                dtexcel.Locale = CultureInfo.CurrentCulture;
                daexcel.Fill(dtexcel);
            }

            conn.Close();
            return dtexcel;
        }
    }
}