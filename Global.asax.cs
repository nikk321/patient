﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace IMUPRO.UI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private const string ROOT_DOCUMENT = "~/login.html";

        protected void Application_Start()
        {
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            //var path = Request.Url.AbsolutePath;
            //var isApi = path.StartsWith("/api", StringComparison.InvariantCultureIgnoreCase);
            //if (isApi)
            //    return;
            //string url = Request.Url.LocalPath;
            //if (!System.IO.File.Exists(Context.Server.MapPath(url)))
            //    Context.RewritePath(ROOT_DOCUMENT, true);
        }
    }
}
