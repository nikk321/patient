﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IMUPRO.UI
{
    public class Patient
    {
        public string PatientName { get; set; }
        public string PatientDOB { get; set; }
        public string SessionCode { get; set; }
        public int IncompatibleFoodsCount { get; set; }
        public DataTable PositiveTest { get; set; }
    }
}