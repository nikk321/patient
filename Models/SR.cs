﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMUPRO.UI
{
    public class SR<T>
    {
        public T Result { get; set; }
        public Exception Error { get; set; }
        public string Info { get; set; }
        public int TotalRows { get; set; }
        public long Identity { get; set; }
        public object Scalar { get; set; }
        public Int16 Success { get; set; }
        public Int64 UserId { get; set; }

    }
}