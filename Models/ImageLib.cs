﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMUPRO.UI
{
    public class ImageLib
    {
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string FileStatus { get; set; }
        public string FileURL { get; set; }
        public string ImageURL { get; set; }
        public bool ValidationStart { get; set; }
    }
}