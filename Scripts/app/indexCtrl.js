﻿//app = angular.module('app', ['cgNotify', 'ngFileUpload']);//set and get the angular module
app.controller('indexController', ['$scope', '$rootScope', '$http', '$window', 'notify', 'Upload', indexController]);

function ValidateImageExtension(files, $rootScope, imgOnly, docsWithImage, $location) {
    var valid = false;

    var fileslength = 0;
    if (files == null || files.length == 0) return;
    var msgInvalidExtension = '';
    var validExtension = ['pdf', 'xls', 'xlsx'];

    for (var i = 0; i < files.length; i++) {
        var fileExtension = files[i].name.split('.').pop();
        for (var ext = 0; ext < validExtension.length; ext++) {
            if (fileExtension.toLowerCase() == validExtension[ext].toLowerCase()) {
                $rootScope.isValid = true;
                valid = true;
                continue;
            }
        }
        if (valid == false)
            msgInvalidExtension += " Invalid extension for " + files[i].name;
        valid == false;
    }
    if (msgInvalidExtension != '') {
        $rootScope.setMsg(msgInvalidExtension);
        return;
    }
}

app.directive('fileModel', ['$parse', '$rootScope', function ($parse, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    ValidateImageExtension(element[0].files, $rootScope);
                    modelSetter(scope, element[0].files);
                });
            });
        }
    };
}]);

//angularjs controller method
function indexController($scope, $rootScope, $http, $window, notify, Upload) {
    $scope.fileList = '';
    $rootScope.isValid = false;
    $scope.isView = false;
    $scope.Patient = {};
    $scope.loading = false;
    $scope.country = [
    {
        "name": "India",
        "id": "1"
    },
    {
        "name": "Singapore",
        "id": "2"
    },
        {
            "name": "Saudi Arabia",
            "id": "3"
        }];
    $scope.validationStart = false;
    $scope.validationText = "Validate";
    $scope.validationCompletes = false;
    $scope.uploading = "Upload";
    $scope.uploadingStarts = false;
    $scope.uploadingCompletes = false;
    $scope.PatientList = [];
    //$rootScope.slimEntities = {};

    $scope.uploadFile = function () {
        debugger;
        if ($scope.countrySelected == undefined) {
            $rootScope.setMsg('Country is mandatory', false);
            return;
        }

        $scope.loading = true;

        var files = $scope.fileList;
        var request = Upload.upload({
            url: '/api/Index/FileUpload',
            data: { file: files },
        });
        var handleImageSuccess = function (res) {
            $scope.loading = false;
            if (res.data.Info == null) {
                $scope.fileList = '';
                $scope.isView = true;
                $scope.ImageLib = res.data.Result;
            }
            else {
                $rootScope.setMsg(data.Info, false)
            }
        };
        var handleImageError = function (res) {
            $scope.loading = false;
        };
        request.then(handleImageSuccess, handleImageError);
    };

    $scope.validation = function () {
        $scope.validationStart = true;
        $scope.validationText = "Validating Files starts Please wait..";

        var count = $scope.ImageLib.length;
        var i = 0;

        var validateFile = function (i) {
            if (i < count) {
                $scope.ImageLib[i].FileStatus = "Validating file..";
                $scope.ImageLib[i].ValidationStart = true;
                $http.get('/api/Index/ValidateFile?fileName=' + $scope.ImageLib[i].FileName).success(function (data) {
                    $scope.ImageLib[i].FileStatus = data.Info == undefined ? 'Done' : data.Info;
                    if (data.Info == undefined) {
                        $scope.PatientList.push(data.Result);
                    }
                    $scope.ImageLib[i].ValidationStart = false;
                    i++;
                    debugger;
                    validateFile(i);
                }).error(function (data) {
                    $scope.ImageLib[i].FileStatus = data.Info;
                    $scope.ImageLib[i].ValidationStart = false;
                    i++;
                    validateFile(i);
                });
            }
        };

        validateFile(i);

        $scope.validationStart = false;
        $scope.validationText = "Validating Completed.";

        $scope.validationCompletes = true;
    };

    $scope.uploadData = function () {
        debugger;
        $scope.uploading = "Uploading data starts Please wait..";
        $scope.uploadingStarts = true;

        $http.post('/api/Index/UploadData', $scope.PatientList).success(function (data) {
            if (data.Info == null) {
                $scope.uploading = "Uploading completed.";
                $scope.uploadingStarts = false;
                $scope.uploadingCompletes = true;
            }
        }).error(function (data) {
            $rootScope.setMsg(data.Info);
        });
    };

    //$scope.getSlimEntities = function () {
    //    $http.post('/api/Index/SlimEntities').success(function (data) {
    //        if (data.Info == undefined)
    //            $rootScope.slimEntities = data.Result;
    //    }).error(function (data) {
    //        $scope.setMsg(data.Info);
    //    });
    //};

    //$scope.getSlimEntities();

    $scope.appliedClass = function (imageDetail) {
        if (imageDetail.FileStatus === 'Success') {
            return 'label-success';
        }
        else {
            if (imageDetail.FileStatus === 'File is not well formatted.') {
                return 'label-danger';
            }
        }
    };
    $rootScope.setMsg = function (msg, succ) {
        notify.closeAll();
        notify({ message: msg, classes: (succ ? "alert-success" : "alert-danger"), duration: 5000 });
    }
}