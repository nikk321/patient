﻿app.controller('loginController', ['$scope', '$http', '$window', 'notify', loginController]);


function loginController($scope, $http, $window, notify) {

    $scope.userId = '';
    $scope.password = '';
    $scope.loading = false;

    $scope.doLogin = function () {

        if ($scope.userId === '') {
            $scope.setMsg('User Id is mandatory', false);
            return;
        }

        if ($scope.password === '') {
            $scope.setMsg('Password is mandatory', false);
            return;
        }

        $scope.loading = true;
        $http.get('/api/Login/AuthenticateUser?userId=' + $scope.userId + '&password=' + $scope.password).success(function (data) {
            //$location.path("/Views/index");
            $window.location.href = "Views/index.html";
        }).error(function (data) {
            $scope.error = "An Error has occured while Login! ";
            $scope.loading = false;
        });

    };

    $scope.setMsg = function (msg, succ) {
        notify.closeAll();
        notify({ message: msg, classes: (succ ? "alert-success" : "alert-danger"), duration: 1000 });
    }
}
