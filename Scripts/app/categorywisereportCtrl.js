﻿app.controller('categorywisereportController', ['$scope', '$rootScope', '$http', '$window', 'notify', 'Upload', categorywisereportController]);


function categorywisereportController($scope, $rootScope, $http, $window, notify, Upload) {
    $scope.rowData = {};
    $scope.dictFood = {};
    $scope.exportText = "Export to Excel";
    $scope.exporting = false;

    $scope.getReportData = function () {
        $http.post('/api/Report/GetCategoryWiseReport').success(function (data) {
            if (data.Info == null) {
                $scope.rowData = data.Result;
            }
        }).error(function (data) {
            $scope.setMsg(data.Info);
        });

    };

    $scope.getReportData();

    $scope.getColumnValue = function (row, columnName, rowIndex) {
        var column = columnName.toString();
        if (rowIndex != undefined) {
            columnName = "c_" + columnName.toString();
        }
        var value = row[columnName];
        var color = 'none';
        switch (value) {
            case "1":
                color = 'yellow';
                break;
            case "2":
                color = 'orange';
                break;
            case "3":
                color = 'red';
                break;
            case "4":
                color = 'brown';
                break;
        }

        if ($('#td' + column).length > 0)
            $($('table > tbody > tr')[rowIndex]).find('#td' + column).css("background-color", color);

        return value;
    };

    $scope.excelUpload = function () {
        $scope.exportText = "Exporting data please wait..";
        $scope.exporting = true;

        $http.post('/api/Report/ExcelExport', $scope.rowData).success(function () {
            $scope.exportText = "Export to Excel";
            $scope.exporting = false;
        }).error(function () {
            $scope.exportText = "Export to Excel";
            $scope.exporting = false;
        });
    };

    $scope.setMsg = function (msg, succ) {
        notify.closeAll();
        notify({ message: msg, classes: (succ ? "alert-success" : "alert-danger"), duration: 5000 });
    }
};