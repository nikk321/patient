﻿using System;
using System.Collections.Generic;
using Dapper;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace IMUPRO.UI
{

    public abstract class DapperBase : IDisposable
    {        
        public SqlConnection Conn { get; set; }
        public DapperBase()
        {
            Conn = GetConnection();
        }
        public DapperBase(string encryptedToken)
        {
            Conn = GetConnection();           
        }

        public SqlConnection GetConnection()
        {
            String connectionString = null;            

            String dbName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DbName"]);
            if (String.IsNullOrWhiteSpace(connectionString))
                connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["IMUPRO"].ToString();            
            try
            {
                var conn = new SqlConnection(connectionString);
                conn.Open();
                conn.Execute(String.Format("Use [{0}]", dbName));
                return conn;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        public void Dispose()
        {
            if (Conn != null && Conn.State != System.Data.ConnectionState.Closed)
            {
                Conn.Close();
                Conn.Dispose();
            }
        }                        
    }
}