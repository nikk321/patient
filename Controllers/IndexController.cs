﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using IMUPRO.UI.BAL;

namespace IMUPRO.UI.Controllers
{
    [RoutePrefix("api/Index")]
    public class IndexController : ApiController
    {
        [HttpPost]
        public SR<Dictionary<string, List<IMUPROSelectList>>> SlimEntities()
        {
            try
            {
                using (var business = new MasterBusiness())
                {
                    return new SR<Dictionary<string, List<IMUPROSelectList>>>() { Identity = 0, Result = business.GetSlimEntities() };
                }
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        public SR<List<ImageLib>> FileUpload()
        {
            try
            {
                List<ImageLib> fileNames = new List<ImageLib>();
                if (!Request.Content.IsMimeMultipartContent())
                {
                    return new SR<List<ImageLib>>() { Identity = 0, Info = "File is not well formatted." };
                }
                else
                {
                    for (int i = 0; i < HttpContext.Current.Request.Files.Count; i++)
                    {
                        var file = HttpContext.Current.Request.Files[i];
                        string ext = file.FileName.Split('.')[1];
                        if (ext.ToLower() == "pdf" || ext.ToLower() == "xls" || ext.ToLower() == "xlsx")
                        {
                            string newFileName = Guid.NewGuid() + "." + ext;
                            string path = HttpContext.Current.Server.MapPath("~/FileUpload/" + newFileName);
                            file.SaveAs(path);

                            string imageURL = "/Images/"; //HttpContext.Current.Server.MapPath("~/Images/");
                            switch (ext)
                            {
                                case "pdf":
                                    imageURL = imageURL + "PictoPDF.jpg";
                                    break;
                                case "xls":
                                    imageURL = imageURL + "excel.png";
                                    break;
                                case "xlsx":
                                    imageURL = imageURL + "excel.png";
                                    break;
                            }

                            fileNames.Add(new ImageLib { FileName = newFileName, FileType = ext, FileStatus = "Pending", FileURL = "/FileUpload/" + newFileName, ImageURL = imageURL });
                        }
                    }
                }

                return new SR<List<ImageLib>>() { Identity = 1, Result = fileNames };

            }
            catch (Exception)
            {
                return new SR<List<ImageLib>>() { Identity = 1, Info = "File is not well formatted." };
            }
        }

        [HttpGet]
        public SR<Patient> ValidateFile(string fileName)
        {
            try
            {
                Patient objPatient = new Patient();
                List<string> fileNames = new List<string>();
                fileNames.Add(fileName);
                string LastExt = fileName.Substring(fileName.IndexOf("."));
                DataTable dtResult = new DataTable();
                string text = string.Empty;
                switch (LastExt)
                {
                    case ".pdf":
                        text = pdfToTable.ExtractTextFromPdf(fileName);
                        objPatient = pdfToTable.PDFData(text);
                        break;

                    case ".xls":
                        dtResult = ExcelToDataTable.ExcelData(fileNames);
                        objPatient = ExcelToDataTable.ExtractDataFromDataTable(dtResult);
                        break;
                    case ".xlsx":
                        dtResult = ExcelToDataTable.ExcelData(fileNames);
                        objPatient = ExcelToDataTable.ExtractDataFromDataTable(dtResult);
                        break;
                }


                return new SR<Patient>() { Identity = 1, Result = objPatient };
            }
            catch (Exception)
            {
                return new SR<Patient>() { Identity = 1, Info = "File is not well formatted." };
            }
        }

        [HttpPost]
        public SR<string> UploadData(List<Patient> lstPatient)
        {
            try
            {
                using (var business = new UploadBusiness())
                {
                    business.UploadData(lstPatient);
                    return new SR<string>() { Identity = 1, Result = "Success" };
                }
            }
            catch (Exception ex)
            {
                return new SR<string>() { Identity = 1, Info = ex.Message };
            }
        }
    }
}
