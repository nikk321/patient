﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace IMUPRO.UI.Controllers
{
    [RoutePrefix("api/Report")]
    public class ReportController : ApiController
    {
        [HttpPost]
        public SR<List<ReportModel>> GetCategoryWiseReport()
        {
            try
            {
                using (var business = new ReportBusiness())
                {
                    return business.GetCategoryWiseReport();
                }
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        public void ExcelExport(List<ReportModel> lstReport)
        {
            try
            {
                string path = HttpContext.Current.Server.MapPath("~/ReportFormats/CategoryWise.xlsx");
                var excelExport = new ExportToExcel(path);
                using (var business = new ReportBusiness())
                {
                    excelExport.WriteDataInSheet(lstReport);
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
